<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function echobase6_calendar_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function echobase6_calendar_node_info() {
  $items = array(
    'event' => array(
      'name' => t('Event'),
      'module' => 'features',
      'description' => t('This content type has a date field, title, and description, and will be automatically listed on a calendar.'),
      'has_title' => '1',
      'title_label' => t('Event Title'),
      'has_body' => '1',
      'body_label' => t('Event Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function echobase6_calendar_views_api() {
  return array(
    'api' => '2',
  );
}
